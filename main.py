import sqlite3
from PySide2.QtCore import QObject
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine

def main():
  app=QGuiApplication()
  engine=QQmlApplicationEngine()
  engine.load("ui.qml")
  app.exec_()

if __name__=="__main__":
  main()
