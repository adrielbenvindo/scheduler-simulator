def insertion(queue,process):
  return queue + [process]

def remove(queue):
  not_short=lambda short,process: process["time"] != short
  short = reduce(queue,lambda process1, process2: process1 if process1["time"] <= process2["time"] else process2)
  return filter(queue,not_short(short))
