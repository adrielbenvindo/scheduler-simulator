def insertion(queue,process):
  return queue + [process]

def remove(queue):
  not_major=lambda major,process: process["priority"] != major
  major = reduce(queue,lambda process1, process2: process1 if process1["priority"] >= process2["priority"] else process2)
  return filter(queue,not_major(major))
