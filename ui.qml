import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

Window{
  id:main
  title:"OS Simulator"
  width:Screen.desktopAvailableWidth
  height:Screen.desktopAvailableHeight
  Column{
    Rectangle{
      id:menu_bar
      color:"grey"
      width:main.width
      height:50
      RowLayout{
        spacing:4
        Button{
          text:"Programs"
        }
        ComboBox{
          model:["FIFO","Round Robin","MLFQ","Preemptive","Short job first"]
        }
        Button{
          text:"start"
        }
      }
    }
    Rectangle{
      color:"light grey"
      width:main.width
      height:main.height-menu_bar.height-status_bar.height
    }
    Rectangle{
      id:status_bar
      color:"black"
      width:main.width
      height:50
    }
  }
}
